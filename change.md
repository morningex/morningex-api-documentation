
# Change Log
 
## [Unreleased - v1.3.0] - 2021-10-TBC
  
### Added
- add break even price in GET /api/positions
- add long/short ratio API
```http
GET /api/products/long_short_ratio HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: 127.0.0.1:8081
User-Agent: HTTPie/2.4.0


```
```http
HTTP/1.1 200 OK
access-control-allow-headers: *
access-control-allow-methods: GET, POST, PUT, DELETE, OPTIONS
access-control-allow-origin: *
content-encoding: gzip
content-type: application/json
date: Mon, 04 Oct 2021 05:53:46 GMT
transfer-encoding: chunked

[
    {
        "long_accounts": "0.5",
        "long_short_ratio": "1",
        "product_id": "btc_usd",
        "short_accounts": "0.5",
        "time": "2021-10-04T05:53:46.349871+00:00"
    },
    {
        "long_accounts": "0",
        "long_short_ratio": "0",
        "product_id": "sol_usdt",
        "short_accounts": "0",
        "time": "2021-10-04T05:53:46.349907+00:00"
    },
    {
        "long_accounts": "0",
        "long_short_ratio": "0",
        "product_id": "eth_usd",
        "short_accounts": "0",
        "time": "2021-10-04T05:53:46.349917+00:00"
    },
    {
        "long_accounts": "0",
        "long_short_ratio": "0",
        "product_id": "dot_usdt",
        "short_accounts": "0",
        "time": "2021-10-04T05:53:46.349927+00:00"
    }
]



```
- add open interest API
```http
GET /api/products/open_interest HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: 127.0.0.1:8081
User-Agent: HTTPie/2.4.0


```
```http
HTTP/1.1 200 OK
access-control-allow-headers: *
access-control-allow-methods: GET, POST, PUT, DELETE, OPTIONS
access-control-allow-origin: *
content-encoding: gzip
content-type: application/json
date: Mon, 04 Oct 2021 05:52:45 GMT
transfer-encoding: chunked

[
    {
        "open_interest": "1000",
        "product_id": "btc_usd"
    },
    {
        "open_interest": "0",
        "product_id": "sol_usdt"
    },
    {
        "open_interest": "0",
        "product_id": "eth_usd"
    },
    {
        "open_interest": "0",
        "product_id": "dot_usdt"
    }
]


```
- add funding rate API
```http
GET /api/funding_rate?id=btc_usd&since=2020-01-01T00%3A00%3A00Z HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: 127.0.0.1:8081
User-Agent: HTTPie/2.4.0


```
```http
HTTP/1.1 200 OK
access-control-allow-headers: *
access-control-allow-methods: GET, POST, PUT, DELETE, OPTIONS
access-control-allow-origin: *
content-encoding: gzip
content-type: application/json
date: Mon, 04 Oct 2021 05:48:06 GMT
transfer-encoding: chunked

[
    {
        "data": "0.999555",
        "time": "2021-10-04T05:15:00.000000+00:00"
    },
    ...
]
```
 
### Changed
- account websocket channel is now sending live message like positions channel instead of periodic message per second
- matches websocket channel includes liquidation messages
 
### Fixed
