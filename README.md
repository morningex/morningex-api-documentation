##### Creating a Request
All REST requests must contain the following headers:

```
ACCESS-KEY The api key as a string.
ACCESS-SIGN The base64-encoded signature (see Signing a Message).
ACCESS-TIMESTAMP A timestamp for your request.
ACCESS-PASSPHRASE The passphrase you specified when creating the API key.
```
All request bodies should be valid JSON.

##### A smaple code from coinbase api documentation that also works with us
```javascript
var crypto = require('crypto');

var secret = 'PYPd1Hv4J6/7x...';

var timestamp = Date.now() / 1000;
var requestPath = '/api/orders';

var body = JSON.stringify({
    price: '1.0',
    size: '1',
    side: 'buy',
    product_id: 'btc_usd'
});

var method = 'POST';

// create the prehash string by concatenating required parts
var what = timestamp + method + requestPath + body;

// decode the base64 secret
var key = Buffer(secret, 'base64');

// create a sha256 hmac with the secret
var hmac = crypto.createHmac('sha256', key);

// sign the require message with the hmac
// and finally base64 encode the result
return hmac.update(what).digest('base64');
```
