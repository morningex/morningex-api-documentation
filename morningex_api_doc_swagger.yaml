openapi: 3.0.1
info:
  title: Morning Exchange Backend API
  description: 'MorningEx API documentation'
  termsOfService: ''
  contact:
    email: info@morning.exchange
  version: "v1.2.0"
servers:
  - url: 'https://api.morning.exchange/api'
tags:
  - name: accounts
  - name: announcements
  - name: currencies
  - name: fees
  - name: fills
  - name: fundings
  - name: indices
  - name: orders
  - name: positions
  - name: products
  - name: server
  - name: withdrawals

paths:
  /accounts:
    get:
      tags:
        - accounts
      operationId: ListAccounts
      description: Get a list of trading accounts associated with the API key.
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Account'
      security:
        - bearerAuth: []
  '/accounts/{account_id}':
    get:
      tags:
        - accounts
      description: Information for a single account. Use this endpoint when you know the account_id. API key must be associated with the account.
      operationId: getAccount
      parameters:
        - $ref: '#/components/parameters/accountID'
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Account'
      security:
        - bearerAuth: []
  '/accounts/{account_id}/ledger':
    get:
      tags:
        - accounts
      description: List account activity associated with the API key. Account activity either increases or decreases your account balance.
      operationId: getAccountLedger
      parameters:
        - $ref: '#/components/parameters/accountID'
        - $ref: '#/components/parameters/paginationStart'
        - $ref: '#/components/parameters/paginationEnd'
        - $ref: '#/components/parameters/paginationLimit'
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ledger'
      security:
        - bearerAuth: []
  '/accounts/{account_id}/holds':
    get:
      tags:
        - accounts
      description: List holds of an account that is associated with the API key. Holds are placed on an account for any active orders, active positions or pending withdraw requests.
      operationId: getAccountHold
      parameters:
        - $ref: '#/components/parameters/accountID'
        - $ref: '#/components/parameters/paginationStart'
        - $ref: '#/components/parameters/paginationEnd'
        - $ref: '#/components/parameters/paginationLimit'
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Hold'
              example:
                - account_id: 1
                  created_at: "2020-01-01T00:00:00.000001+00:00"
                  updated_at: "2020-01-01T00:00:00.000001+00:00"
                  amount: "200.1"
                  type: "positions"
                  ref: "btc_usd"
                - account_id: 1
                  created_at: "2020-01-01T00:00:00.000001+00:00"
                  updated_at: "2020-01-01T00:00:00.000001+00:00"
                  amount: "200.1"
                  type: "orders"
                  ref: 08c16f4f-c37e-11ea-8061-cb12b1811200
                - account_id: 1
                  created_at: "2020-01-01T00:00:00.000001+00:00"
                  updated_at: "2020-01-01T00:00:00.000001+00:00"
                  amount: "200.1"
                  type: "withdrawals"
                  ref: ""
      security:
        - bearerAuth: []
  /currencies:
    get:
      tags:
        - currencies
      description: List supported currencies on the exchange
      operationId: listSupportedCurrencies
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Asset'
  /withdrawals/fees:
    get:
      tags:
        - withdrawals
      description: List the network fee estimation of supported assets
      operationId: withdrawalsFee
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: object
                properties:
                  asset:
                    type: string
                    example: btc
                  fee:
                    type: integer
                    example: 1000
                  blocks:
                    type: integer
                    example: 2
      security:
        - bearerAuth: []
  /fills:
    get:
      tags:
        - fills
      description: Get a list of recent fills associated with the API key.
      operationId: listFills
      parameters:
        - $ref: '#/components/parameters/productIDQuery'
        - name: order_id
          in: query
          schema:
            type: string
        - $ref: '#/components/parameters/paginationStart'
        - $ref: '#/components/parameters/paginationEnd'
        - $ref: '#/components/parameters/paginationLimit'
      responses:
        '200':
          description: success
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Fill'
      security:
        - bearerAuth: []
  /fees:
    get:
      tags:
        - fees
      description: List trading fee specifications of supported products
      operationId: GetFees
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    product:
                      type: string
                      example: "btc_usd"
                    maker_fee_rate:
                      type: string
                      example: "0.0002"
                    taker_fee_rate:
                      type: string
                      example: "0.0007"
      security:
        - bearerAuth: []
  /orders:
    get:
      tags:
        - orders
      description: List your current open orders associated with the API key.
      operationId: getOrders
      parameters:
        - $ref: '#/components/parameters/productIDQuery'
        - $ref: '#/components/parameters/paginationStart'
        - $ref: '#/components/parameters/paginationEnd'
        - $ref: '#/components/parameters/paginationLimit'
        - in: query
          name: status
          schema:
            type: string
            nullable: true
          style: form
          explode: false
          examples:
            single status:
              value: "open"
            multiple  status:
              value: "open,done"
          description: select orders with specific status  ["open", "received", "done"]
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Order'
      security:
        - bearerAuth: []
    post:
      tags:
        - orders
      description: "You can place two types of orders: limit and market. Once an order is placed, your account funds will be put on hold for the duration of the order. How much and which funds are put on hold depends on the order type and parameters specified."
      operationId: postOrders
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Order'
            examples:
              Limit order:
                $ref: '#/components/examples/limitOrder'
              Market order:
                $ref: '#/components/examples/marketOrder'
              Stop limit order:
                $ref: '#/components/examples/stopOrder'
              Detailed limit order:
                $ref: '#/components/examples/detailedLimitOrder'
        required: true
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Order'
      security:
        - bearerAuth: []
    delete:
      tags:
        - orders
      description: With best effort, cancel all open orders associated with the API key. The response is a list of ids of the canceled orders.
      operationId: deleteOrderByProductId
      parameters:
        - $ref: '#/components/parameters/productIDQuery'
      responses:
        '200':
          description: 'order id list'
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                example: [
                  "bed4954e-f4e2-11ea-80f8-e3b584eb8200",
                  "bde84c5c-f4e2-11ea-80f8-4d2d20c37c00"
                ]
      security:
        - bearerAuth: []
  '/orders/{order_id}':
    description: Get a single order by order id associated with the API key.
    parameters:
      - name: order_id
        in: path
        required: true
        schema:
          type: string
    get:
      tags:
        - orders
      description: Get an order on the exchange
      operationId: GetAnOrder
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Order'
      security:
        - bearerAuth: []
    delete:
      tags:
        - orders
      description: Cancel a previously placed order. Order must be associated with the API key.
      operationId: deleteAnOrder
      responses:
        '200':
          description: 'order id'
          content:
            application/json:
              schema:
                type: array
                items:
                  type: string
                example: ["bed4954e-f4e2-11ea-80f8-e3b584eb8200"]
      security:
        - bearerAuth: []
  '/positions/{product_id}':
    get:
      tags:
        - positions
      description: Get information of the trading position of requested product associated with the API key.
      operationId: GetOnePosition
      parameters:
        - $ref: '#/components/parameters/productID'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Position'
      security:
        - bearerAuth: []
  /positions/cross:
    post:
      tags:
        - positions
      description: Enable cross mode of the trading position of the requested products associated with the API key.
      operationId: crossMode
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                product_id:
                  type: string
                  example: btc_usd
        required: true
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Position'
      security:
        - bearerAuth: []
  /positions/isolate:
    post:
      tags:
        - positions
      description: Enable isolated margin and set leverage of the trading positions of the requested products associated with the API key.
      operationId: isolateMode
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                product_id:
                  type: string
                  example: btc_usd
                leverage:
                  type: integer
                  example: 1
                  maximum: 1
        required: true
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Position'
      security:
        - bearerAuth: []
  /positions/margin/transfer:
    post:
      tags:
        - positions
      description: Add margin to or remove margin from trading positions of the requested products associated with the API key.
      operationId: transferMargin
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                product_id:
                  type: string
                  example: btc_usd
                margin:
                  type: integer
                  example: 1
            examples:
              Add margin:
                description: increase the margin in position
                value:
                  margin: 10000
              Remvoe margin:
                description: reduce the margin in position
                value:
                  margin: -10000
        required: true
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Position'
      security:
        - bearerAuth: []
  /positions/risk:
    post:
      tags:
        - positions
      description: Set risk limit of the trading positions of the requested products associated with the API key.
      operationId: SetPositionRiskLimit
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                product_id:
                  type: string
                  example: btc_usd
                limit:
                  type: integer
                  example: 20000000000
                  minimum: 1
        required: true
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Position'
      security:
        - bearerAuth: []
  /products:
    get:
      tags:
        - products
      description: List suppported trading pairs on the exchange
      operationId: listProducts
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Product'
  '/products/{product_id}/book':
    get:
      tags:
        - products
      description: Get a list of open orders for a product. The amount of detail shown can be customized with the level parameter.
      operationId: getProductOrderBook
      parameters:
        - $ref: '#/components/parameters/productID'
        - name: level
          in: query
          schema:
            type: integer
            enum: [1, 2, 3]
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                level1:
                  $ref: '#/components/schemas/Book'
                  # value: |
                  #   '{
                  #     "asks": [],
                  #     "bids": [
                  #       [
                  #         "10000",  // price
                  #         "1",        // total size
                  #         1         // num of orders
                  #       ]
                  #     ]
                  #   }'
                  summary: "level1 order book"
                level2:
                  value: |
                    {
                      "asks": [],
                      "bids": [
                        [
                          "10000",  // price
                          1,        // total size
                          1         // num of orders
                        ],
                        [
                          "9000",  // price
                          2,        // total size
                          2         // num of orders
                        ]
                      ]
                    }
                  summary: "level2 order book"
                level3:
                  value: |
                    {
                      "asks": []
                      "bids": [
                        [
                          "10000",                                // price
                          1,                                      // total size
                          "4942faca-07f2-11eb-8051-c33352868900"  // order id
                        ],
                        [
                          "9000",                                 // price
                          1,                                      // total size
                          "49439d0e-07f2-11eb-8052-5a4c654e8d00"  // order id
                        ],
                        [
                          "9000",                                 // price
                          1,                                      // total size
                          "4bfe74a4-07f2-11eb-8052-4a1753863900"  // order id
                        ]
                      ]
                    }
                  summary: "level3 order book"
  '/products/{product_id}/ticker':
    get:
      tags:
        - products
      description: Snapshot information about the last trade (tick), best bid/ask and 24h volume.
      operationId: getProductTicker
      parameters:
        - $ref: '#/components/parameters/productID'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ticker'
  '/products/{product_id}/trades':
    get:
      tags:
        - products
      description: List the latest trades for a product.
      operationId: getProductTrades
      parameters:
        - $ref: '#/components/parameters/productID'
        - $ref: '#/components/parameters/paginationStart'
        - $ref: '#/components/parameters/paginationEnd'
        - $ref: '#/components/parameters/paginationLimit'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Trade'
  '/products/{product_id}/candles':
    get:
      tags:
        - products
      description: Historic rates for a product. Rates are returned in grouped buckets based on requested granularity.
      operationId: getProductCandles
      parameters:
        - $ref: '#/components/parameters/productID'
        - name: start
          in: query
          description: 'Start time in RFC 3339 (i.e. 2000-01-01T00:00:00+00:00)'
          required: true
          schema:
            type: string
        - name: end
          in: query
          description: 'End time in RFC 3339  (i.e. 2000-01-01T00:00:00+00:00)'
          required: true
          schema:
            type: string
        - name: granularity
          in: query
          description: Desired timeslice in seconds
          required: true
          schema:
            type: integer
            enum: [60, 300, 900, 3600, 21600, 86400]
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Candles'
  '/products/{product_id}/stats':
    get:
      tags:
        - products
      description: Get 24 hr stats for the product. volume is in base currency units. open, high, low are in quote currency units.
      operationId: getProductStats
      parameters:
        - $ref: '#/components/parameters/productID'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: object
                properties:
                  open:
                    type: string
                    example: '10000.1'
                  high:
                    type: string
                    example: '11111'
                  low:
                    type: string
                    example: '9000'
                  volume:
                    type: string
                    example: 1
  /time:
    get:
      tags:
        - server
      description: Get exchange server time
      operationId: getServerTime
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: object
                properties:
                  iso:
                    type: string
                    example: '2020-01-01T00:00:00.000001+00:00'
                  epoch:
                    type: string
                    example: '1577836800'
  /announcements:
    get:
      tags:
        - announcements
      description: Get a list of announcements with information about exchange trading, newsletter, etc.
      operationId: getAnnouncements
      parameters:
        - $ref: '#/components/parameters/since'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    title:
                      type: string
                    body:
                      type: string
                    time:
                      type: string
                      format: date-time
                      example: '2020-01-01T00:00:00.000001+00:00'
  /fundings:
    get:
      tags:
        - fundings
      description: Get a list funding history. Funding are generated from the liquidation and will be used for funding to secure future liquidations.
      operationId: getFunding
      parameters:
        - name: id
          in: query
          required: true
          schema:
            type: string
            enum: ["btc"]
        - $ref: '#/components/parameters/since'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    data:
                      type: string
                      example: 0
                    time:
                        type: string
                        format: date-time
                        example: '2020-01-01T00:00:00.000001+00:00'
  /indices:
    get:
      tags:
        - indices
      description: Get a list price indices on the exchange
      operationId: getIndices
      parameters:
        - name: id
          in: query
          required: true
          schema:
            type: string
            example: btc_usd
        - $ref: '#/components/parameters/since'
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    data:
                      type: string
                      example: 1.0
                    time:
                        type: string
                        format: date-time
                        example: '2020-01-01T00:00:00.000001+00:00'
components:
  schemas:
    Side:
      type: string
      enum: ["buy", "sell"]
    Detail:
      type: object
      properties:
        order_id:
          type: string
          example: 08c16f4f-c37e-11ea-8061-cb12b1811200
        trade_id:
          type: integer
          example: 1
        product_id:
          type: string
          example: btc_usd
    Ledger:
      type: object
      properties:
        id:
          type: integer
          example: 1
        created_at:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
        amount:
          type: string
          example: 1
        type:
          type: string
          example: profits
          enum:
            - profits
        details:
          type: array
          items:
            $ref: '#/components/schemas/Detail'
    Hold:
      type: array
      items:
        type: object
        properties:
          account_id:
            type: integer
          created_at:
            type: string
            format: date-time
          updated_at:
            type: string
            format: date-time
          amount:
            type: string
          type:
            type: string
            enum:
              - positions
              - orders
              - withdrawals
          ref:
            type: string
    Account:
      type: object
      properties:
        id:
          type: integer
          format: uint64
          example: 1
        currency:
          type: string
          example: usdt
        balance:
          type: string
          example: 20000.2
        available:
          type: string
          example: 1.2
        hold:
          type: string
          example: 19999
        update_time:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
    Asset:
      type: object
      properties:
        id:
          type: string
          example: btc
        name:
          type: string
          example: Bitcoin
        min_size:
          type: string
          example: '0.00000001'
    Fill:
      type: object
      properties:
        trade_id:
          type: integer
          example: 1
        product_id:
          type: string
          example: btc_usd
        price:
          type: string
          example: '10000.1'
        size:
          type: string
          example: 100.1
        order_id:
          type: string
          example: 08c16f4f-c37e-11ea-8061-cb12b1811200
        created_at:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
        liquidity:
          type: string
          enum:
            - M
            - T
          description: |
            - M: maker
            - T: taker
        fee:
          type: string
          example: -0.249
        settled:
          type: boolean
          example: true
        side:
          $ref: '#/components/schemas/Side'
    Order:
      required:
        - product_id
        - side
        - size
      type: object
      properties:
        id:
          type: string
          example: 08c16f4f-c37e-11ea-8061-cb12b1811200
          readOnly: true
        price:
          type: string
          # nullable: True
          # description: '*required if type is not market'
          # format: float
          example: 10000.1
        size:
          type: string
          example: 123
        product_id:
          type: string
          example: btc_usd
          enum:
            - btc_usd
        side:
          $ref: '#/components/schemas/Side'
        stp:
          type: string
          nullable: True
          description: |
            - "dc": decrease and cancel
            - "cn": cancel new
            - "co": cancel old
            - "cb": cancel both
          default: dc
          enum:
            - dc
            - cn
            - co
            - cb
            - null
        stop:
          type: string
          nullable: True
          example: entry
          enum:
            - entry
            - loss
            - null
        stop_price:
          type: string
          nullable: True
          # description: '*required if stop is not null'
          # format: float
          example: "10001.2"
        type:
          type: string
          default: limit
          nullable: true
          enum:
            - limit
            - market
            - null
        time_in_force:
          type: string
          nullable: True
          enum:
            - gtc
            - ioc
            - fok
          description: |
            - "gtc": good till cancel
            - "ioc": instance or cancel
            - "fok": fill or kill
            - *"gtt": is coming
          default: gtc
        post_only:
          type: boolean
          default: false
        created_at:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
          readOnly: true
        filled_size:
          type: string
          example: 1
          readOnly: true
        executed_value:
          type: string
          example: 1
          readOnly: true
        status:
          type: string
          example: done
          readOnly: true
        settled:
          type: boolean
          readOnly: true
        reason:
          type: string
          example: canceled
          readOnly: true
    Position:
      type: object
      properties:
        account:
          type: integer
          example: 1
        product_id:
          type: string
          example: btc_usd
        currency:
          type: string
          example: btc
        underlying:
          type: string
          example: BTC
        quote_currency:
          type: string
          example: usd
        commission:
          type: string
          example: "0.0007"
        init_margin_req:
          type: string
          example: "0.01"
        maint_margin_req:
          type: string
          example: "0.005"
        risk_limit:
          type: string
          example: 20000000000
        leverage:
          type: integer
          example: 1
        cross_margin:
          type: boolean
          example: true
        deleverage_percentile:
          type: integer
          example: 0
        rebalanced_pnl:
          type: string
          example: 0
        prev_realized_pnl:
          type: string
          example: 0
        prev_unrealized_pnl:
          type: string
          example: 0
        prev_close_price:
          type: string
          example: 0
        opening_timestamp:
          type: string
          example:
            "2020-01-01T00:00:00.000001+00:00"
        opening_size:
          type: string
          example: 100
        opening_cost:
          type: string
          example: 0
        opening_comm:
          type: string
          example: 0
        open_order_buy_size:
          type: string
          example: 100
        open_order_buy_cost:
          type: string
          example: 0
        open_order_buy_premium:
          type: string
          example: 0
        open_order_sell_size:
          type: string
          example: 0
        open_order_sell_cost:
          type: string
          example: 0
        open_order_sell_premium:
          type: string
          example: 0
        exec_buy_size:
          type: string
          example: 500
        exec_buy_cost:
          type: string
          example: 0
        exec_sell_size:
          type: string
          example: 300
        exec_sell_cost:
          type: string
          example: 0
        exec_size:
          type: string
          example: 800
        exec_cost:
          type: string
          example: 0
        exec_comm:
          type: string
          example: 0
        current_size:
          type: string
          example: 200
        current_cost:
          type: string
          example: 0
        current_comm:
          type: string
          example: 0
        realised_cost:
          type: string
          example: 0
        unrealised_cost:
          type: string
          example: 0
        is_open:
          type: boolean
          example: true
        mark_price:
          type: string
          example: "10000"
        mark_value:
          type: string
          example: 2000000
        pos_margin:
          type: string
          example: 1468867
        pos_maint:
          type: string
          example: 1468867
        avg_entry_price:
          type: string
          example: '13636.3636'
        break_even_price:
          type: string
          example: '0.0000'
        margin_call_price:
          type: string
          example: '6835.2692'
        liquidation_price:
          type: string
          example: '6835.2692'
        bankrupt_price:
          type: string
          example: '6818.1810'
        last_price:
          type: string
          example: '9000.0000'
        last_value:
          type: string
          example: 3333333
        realised_pnl:
          type: string
          example: -1133333
        unrealised_pnl:
          type: string
          example: -1133333
        timestamp:
          type: string
          example:
            "2020-01-01T00:00:00.000001+00:00"
    Product:
      type: object
      properties:
        id:
          type: string
          example: btc_usd
        display_name:
          type: string
          example: BTC-USD
        base_currency:
          type: string
          example: btc
        quote_currentcy:
          type: string
          example: usd
        quote_increment:
          type: string
          example: 0.1
        base_min_size:
          type: string
          example: 1
        base_max_size:
          type: string
          example: 10000000
        max_price:
          type: string
          example: "1000000"
        init_margin:
          type: string
          example: "0.01"
        maint_margin:
          type: string
          example: "0.005"
        risk_base_size:
          type: string
          example: 20000000000
        risk_step_size:
          type: string
          example: 5000000000
        status:
          type: string
          example: "online"
        status_message:
          type: string
          example: ""
        trading_disabled:
          type: boolean
          example: false
        multiplier:
          type: integer
          example: -1
        is_inverse:
          type: boolean
          example: True
        is_quanto:
          type: boolean
          example: False
        settle_currency:
          type: string
          example: 'btc'
        funding_rate:
          type: string
          example: 0.0
        next_funding_time:
          type: string
          example:
            "2020-01-01T00:00:00.000000+00:00"
        predicted_funding_rate:
          type: string
          example: 0.0
    Ticker:
      type: object
      properties:
        product_id:
          type: string
          example: 'btc_usd'
        best_bid:
          type: string
          example: '9000'
        best_ask:
          type: string
          example: '11000'
        trade_id:
          type: integer
        side:
          type: string
          example: 'sell'
        last_size:
          type: string
          example: 100
        price:
          type: string
          example: '10000'
        high_24h:
          type: string
          example: '10000'
        low_24h:
          type: string
          example: '10000'
        open_24h:
          type: string
          example: '10000'
        volume_24h:
          type: string
          example: 100
        mark_price:
          type: string
          example: '10000'
        time:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
    Trade:
      type: object
      properties:
        time:
          type: string
          format: date-time
          example: "2020-01-01T00:00:00.000001+00:00"
        trade_id:
          type: integer
          example: 1
        price:
          type: string
          example: '10000.1'
        size:
          type: string
          example: 100
        side:
          type: string
          description: taker side
          example: buy
    Candles:
      type: array
      description: '[time, low , high, open, close, volume]'
      items:
        type: number
      example: [
        "#[time, low , high, open, close, volume]",
        [1594356700, 9000.1, 12000.3, 10000, 11000.5, 300]]
    Book: {
        "ask": [
          "price",
          "total size",
          "number of orders"
        ],
        "bids": [
          "10000",
          "1",
          "1"
        ]
      }
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  parameters:
    accountID:
      name: account_id
      in: path
      required: true
      schema:
        type: integer
    productID:
      name: product_id
      in: path
      required: true
      schema:
        type: string
        enum: ["btc_usd"]
    productIDQuery:
      name: product_id
      in: query
      schema:
        type: string
        enum: ["btc_usd"]
    paginationStart:
      name: start
      in: query
      description: Input ID (integer) or Time (RFC 3339)
      schema:
        oneOf:
          - type: integer
          - type: string
    paginationEnd:
      name: end
      in: query
      description: Input ID (integer) or Time (RFC 3339)
      schema:
        oneOf:
          - type: integer
          - type: string
    paginationLimit:
      name: limit
      in: query
      description: Number of results per request. Maximum 100.
      schema:
        type: integer
        default: 100
        minimum: 1
        maximum: 100
    since:
      name: since
      in: query
      description: 'Time in RFC 3339 (i.e. 2000-01-01T00:00:00+00:00)'
      schema:
        type: string
  examples:
    limitOrder:
      description: the simplest limit order
      value:
        type: limit
        side: buy
        product_id: btc_usd
        size: 100
        price: 10000
    marketOrder:
      description: the simplest market order
      value:
        type: market
        side: buy
        product_id: btc_usd
        size: 100
    stopOrder:
      description: a stop limit order
      value:
        type: limit
        side: buy
        product_id: btc_usd
        size: 100
        price: 10000
        stop: loss
        stop_price: 10000
    detailedLimitOrder:
      description: a detailed limit order
      value:
        type: limit
        side: buy
        product_id: btc_usd
        size: 100
        price: 10000
        stp: 'dc'
        time_in_force: 'gtc'
        post_only: true
