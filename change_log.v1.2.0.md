### Restful

<details close>

#### Account API

```
GET /api/accounts
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "available": "30000000000", // from integer to string
        "balance": "30000000000", // from integer to string
        "currency": "btc",
        "hold": "0", // from integer to string
        "id": 2,
        "update_time": "2021-08-30T09:55:31.480653+00:00"
    }
]
```

</details>

```
GET /api/accounts/{aid}
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "available": "30000000000", // from integer to string
    "balance": "30000000000", // from integer to string
    "currency": "btc",
    "hold": "0", // from integer to string
    "id": 2,
    "update_time": "2021-08-30T09:55:31.480653+00:00"
}
```

</details>

```
GET /api/accounts/{aid}/ledger
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "amount": "-2000", // from integer to string
        "created_at": "2021-08-30T09:57:36.619831+00:00",
        "details": {
            "order_id": "b402c33e-0978-11ec-8000-bf6f1a5e7100",
            "product_id": "btc_usd",
            "trade_id": 30
        },
        "id": 8,
        "type": "profits"
    }
]
```

</details>

```
GET /api/accounts/{aid}/holds
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "account_id": 2,
        "amount": "10014000", // from integer to string
        "created_at": "2021-08-30T09:55:31.484974+00:00",
        "ref": "btc_usd",
        "type": "positions",
        "updated_at": "2021-08-30T09:57:36.624668+00:00"
    }
]
```

</details>

#### Position API

```
GET /api/positions/{product_id}
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "account": 2,
    "avg_entry_price": "10000.0000",
    "bankrupt_price": "5000.0000",
    "break_even_price": "0.0000",
    "commission": "0.0007",
    "cross_margin": false,
    "currency": "btc",
    "current_comm": "0", // from integer to string
    "current_cost": "0", // from integer to string
    "current_size": "1000", // from integer to string
    "deleverage_percentile": 0,
    "exec_buy_cost": "0", // from integer to string
    "exec_buy_size": "1000", // from integer to string
    "exec_comm": "0", // from integer to string
    "exec_cost": "0", // from integer to string
    "exec_sell_cost": "0", // from integer to string
    "exec_sell_size": "0", // from integer to string
    "exec_size": "1000", // from integer to string
    "init_margin_req": "0.01",
    "is_open": true,
    "last_price": "10000.0000",
    "last_value": "10000000", // from integer to string
    "leverage": 1,
    "liquidation_price": "5012.5313",
    "maint_margin_req": "0.005",
    "margin_call_price": "5012.5313",
    "mark_price": "10000",
    "mark_value": "10000000", // from integer to string
    "open_order_buy_cost": "0", // from integer to string
    "open_order_buy_premium": "0", // from integer to string
    "open_order_buy_size": "1000", // from integer to string
    "open_order_sell_cost": "0", // from integer to string
    "open_order_sell_premium": "0", // from integer to string
    "open_order_sell_size": "0", // from integer to string
    "opening_comm": "0", // from integer to string
    "opening_cost": "0", // from integer to string
    "opening_size": "1000", // from integer to string
    "opening_timestamp": "2021-08-30T09:57:36.624559+00:00",
    "pos_maint": "50000", // from integer to string
    "pos_margin": "10014000", // from integer to string
    "prev_close_price": "0",
    "prev_realized_pnl": "0", // from integer to string
    "prev_unrealized_pnl": "0", // from integer to string
    "product_id": "btc_usd",
    "quote_currency": "usd",
    "realised_cost": "0", // from integer to string
    "realised_pnl": "-2000", // from integer to string
    "rebalanced_pnl": "0", // from integer to string
    "risk_limit": "20000000000", // from integer to string
    "timestamp": "2021-08-30T10:02:12.124445+00:00",
    "underlying": "BTC",
    "unrealised_cost": "0", // from integer to string
    "unrealised_pnl": "0" // from integer to string
}
```

</details>

```
POST /api/positions/cross
```
same as get positions

```
POST /api/positions/isolate
```
same as get positions

```
POST /api/positions/margin
```
same as get positions

```
POST /api/positions/risk
```
same as get positions

#### Order API

```
POST /api/orders
```

<details close>
<summary>Sample request</summary>

- order size input accept floats for linear contract
- add reduce_only flag: reduce_only orders can only reduce position size and will auto adjust order size or cancel when position size change.

```javascript
{
    "product_id": "btc_usdt",
    "side": "buy",
    "size": "0.1", // unit: usdt
    "price": "10000.1",
    "reduce_only": true
}
```

</details>

<details close>
<summary>Sample response</summary>

```javascript
{
    "created_at": "2021-08-30T10:09:08.337287+00:00",
    "executed_value": "0", // from integer to string
    "filled_size": "0", // from integer to string
    "id": "5087b0e2-097a-11ec-8004-e6cc5949c300",
    "post_only": false,
    "price": "10000",
    "product_id": "btc_usd",
    "reduce_only": false,
    "settled": true,
    "side": "buy",
    "size": "1", // from integer to string
    "status": "pending",
    "stp": "dc",
    "time_in_force": "gtc",
    "type": "limit"
}
```

</details>

```
GET /api/orders
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "created_at": "2021-08-30T10:09:08.337287+00:00",
        "executed_value": "0", // from integer to string
        "filled_size": "0", // from integer to string
        "id": "5087b0e2-097a-11ec-8004-e6cc5949c300",
        "post_only": false,
        "price": "10000",
        "product_id": "btc_usd",
        "reduce_only": false,
        "settled": true,
        "side": "buy",
        "size": "1", // from integer to string
        "status": "open",
        "stp": "dc",
        "time_in_force": "gtc",
        "type": "limit"
    }
]
```

</details>

```
GET /api/orders/{oid}
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "created_at": "2021-08-30T10:09:08.337287+00:00",
    "executed_value": "0", // from integer to string
    "filled_size": "0", // from integer to string
    "id": "5087b0e2-097a-11ec-8004-e6cc5949c300",
    "post_only": false,
    "price": "10000",
    "product_id": "btc_usd",
    "reduce_only": false,
    "settled": true,
    "side": "buy",
    "size": "1", // from integer to string
    "status": "open",
    "stp": "dc",
    "time_in_force": "gtc",
    "type": "limit"
}
```

</details>

#### Fill API

```
GET /api/fills
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "created_at": "2021-08-30T10:11:03.224814+00:00",
        "fee": "20", // from integer to string
        "liquidity": "M",
        "order_id": "Deleverages",
        "price": "1000000",
        "product_id": "btc_usd",
        "settled": true,
        "side": "sell",
        "size": "1000", // from integer to string
        "trade_id": 49
    }
]
```

</details>

#### Product API

```
GET /api/products/{product_id}/book
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "asks": [],
    "bids": [
        [
            "10000",
            "1", // from integer to string
            1
        ]
    ]
}
```

</details>

```
GET /api/products/{product_id}/ticker
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "best_ask": "0",
    "best_bid": "10000",
    "high_24h": "10000",
    "last_size": "1000", // from integer to string
    "low_24h": "10000",
    "mark_price": "10000",
    "open_24h": "10000",
    "price": "10000",
    "product_id": "btc_usd",
    "side": "sell",
    "time": "2021-08-30T10:07:35.357911+00:00",
    "trade_id": 18,
    "volume_24h": "1000" // from integer to string
}
```

</details>

```
GET /api/products/{product_id}/candles
```

<details close>
<summary>Sample response</summary>

```javascript
[
    [
        1630318020,
        10000.0,
        10000.0,
        10000.0,
        10000.0,
        1000.0 // from integer to float
    ]
]
```

</details>

```
GET /api/products/{product_id}/stats
```

<details close>
<summary>Sample response</summary>

```javascript
{
    "high": "10000",
    "low": "10000",
    "open": "10000",
    "volume": "1000" // from integer to string
}
```

</details>

```
GET /api/products/{product_id}/trades
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "price": "10000",
        "side": "sell",
        "size": "1000", // from integer to string
        "time": "2021-08-30T10:07:35.357911+00:00",
        "trade_id": 18
    }
]
```

</details>

#### Index API

```
GET /api/indices
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "data": "10001", // from integer to string
        "time": "2021-08-30T10:15:50.000000+00:00"
    }
]
```

</details>

#### Funding API

```
GET /api/fundings
```

<details close>
<summary>Sample response</summary>

```javascript
[
    {
        "data": "113930", // from integer to string
        "time": "2021-08-30T10:16:03.571368+00:00"
    }
]
```

</details>
</details>

### WS

<details close>

```
ticker
```

<details close>
<summary>sample message</summary>

```javascript
{
    "type": "ticker",
    "sequence": 0,
    "product_id": "btc_usd",
    "best_bid": "0",
    "best_ask": "0",
    "trade_id": 1,
    "side": "sell",
    "last_size": "1", // from integer to string
    "price": "10000",
    "high_24h": "10000",
    "low_24h": "10000",
    "open_24h": "10000",
    "volume_24h": "2", // from integer to string
    "time": "2020-01-01T00:00:00.000000001+00:00"
}
```

</details>

```
level2
```

<details close>
<summary>snapshot</summary>

```javascript
{
    "type": "snapshot",
    "product_id": "btc_usd",
    "bids": [
        [
            "price",
            "size"
        ]
    ],
    "asks": [
        [
            "12000",
            "10" // from integer to string
        ]
    ]
}
```

</details>

<details close>
<summary>update</summary>

```javascript
{
    "type": "update",
    "product_id": "btc_usd",
    "changes": [
        [
            "side",
            "price",
            "size"
        ],
        [
            "buy",
            "10000",
            "1" // from integer to string
        ]
    ],
    "time": "2020-01-01T00:00:00.000001+00:00"
}
```

</details>

```
full
```

<details close>
<summary>received</summary>

```javascript
{
    "type": "received",
    "sequence": 0,
    "product_id": "btc_usd",
    "order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
    "side": "buy",
    "order_type": "limit",
    "price": "10000",
    "size": "1", // from integer to string
    "time": "2020-01-01T00:00:00.000001+00:00"
}
```

</details>

<details close>
<summary>open</summary>

```
order posted into order book.
```

```javascript
{
    "type": "open",
    "sequence": 0,
    "product_id": "btc_usd",
    "order_id": "e2b9c973-0d6f-11eb-8000-aa6c54ba2400",
    "side": "buy",
    "price": "10000",
    "remaining_size": "1", // from integer to string
    "time": "2020-01-01T00:00:00.000001+00:00"
}   
```

</details>  

<details close>
<summary>match</summary>

```javascript
{
    "type": "match",
    "sequence": 0,
    "trade_id": 6,
    "maker_order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
    "taker_order_id": "93ed0060-0d6f-11eb-8000-460676538000",
    "time": "2020-01-01T00:00:00.000001+00:00",
    "product_id": "btc_usd",
    "size": "1", // from integer to string
    "price": "10000",
    "side": "buy"
}
```

</details>

<details close>
<summary>done</summary>

```javascript
{
    "type": "done",
    "sequence": 0,
    "product_id": "btc_usd",
    "order_id": "93ed0060-0d6f-11eb-8000-460676538000",
    "side": "sell",
    "price": "10000",
    "remaining_size": "0", // from integer to string
    "reason": "filled",
    "time": "2020-01-01T00:00:00.000001+00:00"
}
```
</details>  

```
users
```

<details close>
<summary>activate</summary>

```javascript
{
    "type": "activate",
    "product_id": "btc_usd",
    "order_id": "e2b9c973-0d6f-11eb-8000-aa6c54ba2400",
    "stop_type": "entry",
    "side": "buy",
    "stop_price": "9999",
    "limit_price": "10000",
    "size": "1", // from integer to string
    "time": "2020-01-01T00:00:00.000001+00:00"
}    
```

</details>

<details close>
<summary>change</summary>

```javascript
{
    "type": "change",
    "sequence": 0,
    "product_id": "btc_usd",
    "order_id": "b3de9d65-0d70-11eb-8000-44f6dd984800",
    "side": "sell",
    "price": "10000",
    "new_size": "4", // from integer to string
    "old_size": "5", // from integer to string
    "time": "2020-01-01T00:00:00.000001+00:00"
}    
```

</details>  

```
matches
```

<details close>
<summary>match</summary>

```javascript
{
    "type": "match",
    "sequence": 0,
    "trade_id": 6,
    "maker_order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
    "taker_order_id": "93ed0060-0d6f-11eb-8000-460676538000",
    "time": "2020-01-01T00:00:00.000001+00:00",
    "product_id": "btc_usd",
    "size": "1", // from integer to string
    "price": "10000",
    "side": "buy"
}
```

</details>  

```
positions
```

<details close>
<summary>Sample message</summary>

```javascript
{
    "type": "positions",
    "account": 76,
    "product_id": "btc_usd",
    "currency": "btc",
    "underlying": "BTC",
    "quote_currency": "usd",
    "commission": "0.00075",
    "init_margin_req": "0.01",
    "maint_margin_req": "0.005",
    "risk_limit": "20000000000", // from integer to string
    "leverage": 1,
    "cross_margin": false,
    "deleverage_percentile": 0,
    "rebalanced_pnl": "0", // from integer to string
    "prev_realized_pnl": "0", // from integer to string
    "prev_unrealized_pnl": "0", // from integer to string
    "prev_close_price": "0",
    "opening_timestamp": "null",
    "opening_size": "0", // from integer to string
    "opening_cost": "0", // from integer to string
    "opening_comm": "0", // from integer to string
    "open_order_buy_size": "0", // from integer to string
    "open_order_buy_cost": "0", // from integer to string
    "open_order_buy_premium": "0", // from integer to string
    "open_order_sell_size": "0", // from integer to string
    "open_order_sell_cost": "0", // from integer to string
    "open_order_sell_premium": "0", // from integer to string
    "exec_buy_size": "0", // from integer to string
    "exec_buy_cost": "0", // from integer to string
    "exec_sell_size": "0", // from integer to string
    "exec_sell_cost": "0", // from integer to string
    "exec_size": "0", // from integer to string
    "exec_cost": "0", // from integer to string
    "exec_comm": "0", // from integer to string
    "current_size": "0", // from integer to string
    "current_cost": "0", // from integer to string
    "current_comm": "0", // from integer to string
    "realised_cost": "0", // from integer to string
    "unrealised_cost": "0", // from integer to string
    "is_open": false,
    "mark_price": "10000",
    "mark_value": "0", // from integer to string
    "pos_margin": "0", // from integer to string
    "pos_maint": "0", // from integer to string
    "avg_entry_price": "0.0000",
    "break_even_price": "0.0000",
    "margin_call_price": "0.0000",
    "liquidation_price": "0.0000",
    "bankrupt_price": "0.0000",
    "last_price": "0.0000",
    "last_value": "0", // from integer to string
    "realised_pnl": "0", // from integer to string
    "unrealised_pnl": "0", // from integer to string
    "timestamp": "2020-01-01T00:00:00.000001+00:00"
}
```

</details>  

```
accounts
```

<details close>
<summary>Sample message</summary>

```javascript
{
    "type": "account",
    "id": 1,
    "currency": "btc",
    "available": "30000000000", // from integer to string
    "update_time": "2020-01-01T00:00:00.000001+00:00"
}
```
</details>

</details>
