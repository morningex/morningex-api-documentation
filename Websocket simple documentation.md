### Websocket simple documentation
---
# Overview
- [subscribe](#subscribe)

Websocket channels list:
- [heartbeat](#heartbeat)
- [ticker](#ticker)
- [level2](#level2)
- [full](#full)
- [users](#users)
- [matches](#matches)
- [positions](#positions)
- [accounts](#accounts)

---
## <a name="subscribe"></a>subscribe
The subscribe message example:
```
{
    "type": "subscribe"
    "channels": [
        {
            "name": "users",
            "product_ids": [
                "btc_usd",
                "eth_usd"
            ]
        },
        {
            "name": "ticker",
            "product_ids": [
                "btc_usd"
            ]
        }
    ],
    "signature"': signature,
    "timestamp": timestamp,
    "key": key,
    "passphrase": passphrase,
}
```
Some channels required the authentication like `users`, `positions` and `accounts`.

ps. the message in signature = timestamp + 'GET/users/self/verify'

---
## <a name="heartbeat"></a>heartbeat
Information:
```json
{
    "channel name": "heartbeat",
    "auth": false,
    "description": "send a heartbeat message every second"
}
```
<details close>
<summary>Sample message</summary>

```json
{
    "type": "heartbeat",
    "sequence": 0,
    "product_id": "btc_usd",
    "last_trade_id": 0,
    "time": "2020-01-01T00:00:00.000000001+00:00"
}
```
</details>  

## <a name="ticker"></a>ticker
Information:
```json
{
    "channel name": "ticker",
    "auth": false,
    "description": "send a ticker message for every trade"
}
```
<details close>
<summary>Sample message</summary>

```json
{
    "type": "ticker",
    "sequence": 0,
    "product_id": "btc_usd",
    "best_bid": "0",
    "best_ask": "0",
    "trade_id": 1,
    "side": "sell",
    "last_size": "1",
    "price": "10000",
    "high_24h": "10000",
    "low_24h": "10000",
    "open_24h": "10000",
    "volume_24h": "2",
    "time": "2020-01-01T00:00:00.000000001+00:00"
}
```

</details>  


## <a name="level2"></a>level2

Information:
```json
{
    "channel name": "level2",
    "auth": false,
    "description": "send the snapshot immediately and the change for level2 orderbook"
}
```
<details open>
<summary>Sample message</summary>

- <details close>
    <summary>snapshot</summary>

    ```json
    {
        "type": "snapshot",
        "product_id": "btc_usd",
        "bids": [
            [
                "price",
                "size"
            ]
        ],
        "asks": [
            [
                "12000",
                "10"
            ]
        ]
    }
    ```
    </details>  
- <details close>
    <summary>update</summary>

    ```json
    {
        "type": "update",
        "product_id": "btc_usd",
        "changes": [
            [
                "side",
                "price",
                "size"
            ],
            [
                "buy",
                "10000",
                "1"
            ]
        ],
        "time": "2020-01-01T00:00:00.000001+00:00"
    }
    ```
    </details>  
</details>  

## <a name="full"></a>full
Information:
```json
{
    "channel name": "full",
    "auth": false,
    "description": ":received, open, match and done"
}
```
<details open>
<summary>Sample message</summary>

- <details close>
    <summary>received</summary>

    ```
    order is received by the exchange. recevied order doesn"t implies it is a valid order (insufficient balance, limitation of open order, ...... )
    ```
    ```json
    {
        "type": "received",
        "sequence": 0,
        "product_id": "btc_usd",
        "order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
        "side": "buy",
        "order_type": "limit",
        "price": "10000",
        "size": "1",
        "time": "2020-01-01T00:00:00.000001+00:00"
    }
    ```
    </details>  
- <details close>
    <summary>open</summary>

    ```
    order posted into order book.
    ```
    ```json
    {
        "type": "open",
        "sequence": 0,
        "product_id": "btc_usd",
        "order_id": "e2b9c973-0d6f-11eb-8000-aa6c54ba2400",
        "side": "buy",
        "price": "10000",
        "remaining_size": "1",
        "time": "2020-01-01T00:00:00.000001+00:00"
    }   
    ```
    </details>  
- <details close>
    <summary>match</summary>

    ```json
    {
        "type": "match",
        "sequence": 0,
        "trade_id": 6,
        "maker_order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
        "taker_order_id": "93ed0060-0d6f-11eb-8000-460676538000",
        "time": "2020-01-01T00:00:00.000001+00:00",
        "product_id": "btc_usd",
        "size": "1",
        "price": "10000",
        "side": "buy"
    }
    ```
    </details>  
- <details close>
    <summary>done</summary>

    ```json
    {
        "type": "done",
        "sequence": 0,
        "product_id": "btc_usd",
        "order_id": "93ed0060-0d6f-11eb-8000-460676538000",
        "side": "sell",
        "price": "10000",
        "remaining_size": "0",
        "reason": "filled",
        "time": "2020-01-01T00:00:00.000001+00:00"
    }
    ```
    </details>  
</details>  


## <a name="users"></a>users
Information:
```json
{
    "channel name": "users",
    "auth": true,
    "description": "the messages in full channel but ony receive the message relevant to the user. In addition, it has two more types includeing activate and change."
}
```
<details open>
<summary>Sample message</summary>

- <details close>
    <summary>activate</summary>

    ```
    a stop order is received.
    ```
    ```json
    {
        "type": "activate",
        "product_id": "btc_usd",
        "order_id": "e2b9c973-0d6f-11eb-8000-aa6c54ba2400",
        "stop_type": "entry",
        "side": "buy",
        "stop_price": "9999",
        "limit_price": "10000",
        "size": "1",
        "time": "2020-01-01T00:00:00.000001+00:00"
    }    
    ```
    </details>  
- <details close>
    <summary>change</summary>

    ```
    The order changed due to self-trade prevention (user posted two orders and they are matched with each other.
    ```
    ```json
    {
        "type": "change",
        "sequence": 0,
        "product_id": "btc_usd",
        "order_id": "b3de9d65-0d70-11eb-8000-44f6dd984800",
        "side": "sell",
        "price": "10000",
        "new_size": "4",
        "old_size": "5",
        "time": "2020-01-01T00:00:00.000001+00:00"
    }    
    ```
    </details>  
</details>  


## <a name="matches"></a>matches
Information:
```json
{
    "channel name": "matches",
    "auth": false,
    "description": "the matching message which is automatically included in the users and full channel"
}
```

<details open>
<summary>Sample message</summary>

- <details close>
    <summary>match</summary>

    ```json
    {
        "type": "match",
        "sequence": 0,
        "trade_id": 6,
        "maker_order_id": "93ec0ca3-0d6f-11eb-8000-ada0c95af300",
        "taker_order_id": "93ed0060-0d6f-11eb-8000-460676538000",
        "time": "2020-01-01T00:00:00.000001+00:00",
        "product_id": "btc_usd",
        "size": "1",
        "price": "10000",
        "side": "buy"
    }
    ```
    </details>  
</details>  


## <a name="positions"></a>positions
Information:
```json
{
    "channel name": "positions",
    "auth": true,
    "description": "send a message if position is changed"
}
```
<details close>
<summary>Sample message</summary>

```json
{
    "type": "positions",
    "account": 76,
    "product_id": "btc_usd",
    "currency": "btc",
    "underlying": "BTC",
    "quote_currency": "usd",
    "commission": "0.00075",
    "init_margin_req": "0.01",
    "maint_margin_req": "0.005",
    "risk_limit": "20000000000",
    "leverage": 1,
    "cross_margin": false,
    "deleverage_percentile": 0,
    "rebalanced_pnl": "0",
    "prev_realized_pnl": "0",
    "prev_unrealized_pnl": "0",
    "prev_close_price": "0",
    "opening_timestamp": "null",
    "opening_size": "0",
    "opening_cost": "0",
    "opening_comm": "0",
    "open_order_buy_size": "0",
    "open_order_buy_cost": "0",
    "open_order_buy_premium": "0",
    "open_order_sell_size": "0",
    "open_order_sell_cost": "0",
    "open_order_sell_premium": "0",
    "exec_buy_size": "0",
    "exec_buy_cost": "0",
    "exec_sell_size": "0",
    "exec_sell_cost": "0",
    "exec_size": "0",
    "exec_cost": "0",
    "exec_comm": "0",
    "current_size": "0",
    "current_cost": "0",
    "current_comm": "0",
    "realised_cost": "0",
    "unrealised_cost": "0",
    "is_open": false,
    "mark_price": "10000",
    "mark_value": "0",
    "pos_margin": "0",
    "pos_maint": "0",
    "avg_entry_price": "0.0000",
    "break_even_price": "0.0000",
    "margin_call_price": "0.0000",
    "liquidation_price": "0.0000",
    "bankrupt_price": "0.0000",
    "last_price": "0.0000",
    "last_value": "0",
    "realised_pnl": "0",
    "unrealised_pnl": "0",
    "timestamp": "2020-01-01T00:00:00.000001+00:00"
}
```

</details>  

## <a name="accounts"></a>accounts
Information:
```json
{
    "channel name": "accounts",
    "auth": true,
    "description": "send a message every second"
}
```
<details close>
<summary>Sample message</summary>

```json
{
    "type": "account",
    "id": 1,
    "currency": "btc",
    "available": "30000000000",
    "update_time": "2020-01-01T00:00:00.000001+00:00"
}
```
</details>  
